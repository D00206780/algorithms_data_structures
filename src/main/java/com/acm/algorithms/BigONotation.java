/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acm.algorithms;

import java.util.Arrays;

/**
 * Big O Notation is the measure of how complex a program becomes as the input
 * (n) becomes arbitrarily larger.
 *
 * @author james
 */
public class BigONotation {

    /**
     * O(1) This is called constant time. This method only prints one value. It
     * is non-reliant on the size of the array.
     *
     * @param array
     */
    public static void printFirst(int[] array) {
        System.out.println("\nPrint first element in the array:");
        System.out.println(array[0]);
    }

    /**
     * 0(n) This is called linear time. n = number of elements in the array. If
     * there are 10 elements in the array, we would have to print 10 times. If
     * there are 100 elements in the array, we would have to print 100 times.
     *
     * @param array
     */
    public static void printAll(int[] array) {
        System.out.println("\nPrint all elements in the array:");
        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println("");
    }

    /**
     * 0(n^2) This is called quadratic time. n = number of elements in the
     * array. Here we are nesting two loops. If our array has n items, our outer
     * loop will run n times. Every time our outer loop runs, our inner loop
     * will run n times. This results in executing n times for every n element
     * This is the same as saying n * n or n^2. If there are 10 elements in the
     * array, we would print 100 items.
     *
     * @param array
     */
    public static void printPairs(int[] array) {
        System.out.println("\nPrint pairs:");
        for (int firstElement : array) {
            for (int secondElement : array) {
                int[] orderedPair = new int[]{firstElement, secondElement};
                System.out.println(Arrays.toString(orderedPair));
            }
        }
    }

    /**
     * O(n) + O(n) = 0(n) When calculating the Big O complexity of something, we
     * can remove the constants. This is because we are only concerned by how
     * the function will grow as the input gets larger.
     *
     * @param array
     */
    public static void printElementsTwice(int[] array) {
        System.out.println("\nPrint all elements twice:");
        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println("");

        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println("");
    }

    /**
     * O(n!) This is called n factorial. This is the worst possible time. If
     * given a value n, this function will run n! times.
     *
     * @param n
     */
    public static void nFactorial(int n) {
        int nFactorial = 1;
        for (int i = 1; i <= n; i++) {
            nFactorial = nFactorial * i;
        }
        System.out.println(nFactorial);

        for (int i = 1; i <= nFactorial; i++) {
            System.out.println(i);
            //This would then run nFactorial times. 
        }
    }

    /**
     * Main driver
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        printFirst(array);
        printAll(array);
        printPairs(array);
        printElementsTwice(array);

        int n = 10;
        System.out.print("\nn!: ");
        nFactorial(n);
    }
}