/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acm.algorithms;

import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author James
 *
 * Clarifying questions - Will array be sorted? Will the array ever be empty?
 * Will there always be a solution?
 *
 */
public class TwoSum {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] nums = new int[]{2, 7, 11, 15};
        System.out.println(Arrays.toString(twoSumSlow(nums, 9)));
        System.out.println(Arrays.toString(twoSumQuick(nums, 22)));
    }

    /**
     * Complexity: O(n^2)
     *
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSumSlow(int[] nums, int target) {
        int array[] = new int[2];
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = i; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    array = new int[]{i, j};
                }
            }
        }
        return array;
    }

    /**
     * Complexity O(n)
     *
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSumQuick(int[] nums, int target) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (hm.containsKey(target - nums[i])) {
                return new int[]{hm.get(target - nums[i]), i};
            }
            hm.put(nums[i], i);
        }
        return new int[]{-1, -1};
    }
}