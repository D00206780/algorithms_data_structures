/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acm.algorithms;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author James
 */
public class StacksAndQueues {

    /**
     * Stacks work with a LIFO process. This means that the last element added
     * to the stack, will be the first element removed from the stack. We use
     * the push() method to add to a stack, and the pop() method to remove from
     * a stack. We can also use the peek() method to read the last element added
     * to the the stack.
     */
    public static void stack() {
        Stack<String> stack = new Stack();

        stack.push("Hello");
        stack.push(" ");
        stack.push("World");
        stack.push("!");

        System.out.print(stack.pop());
        System.out.print(stack.pop());
        System.out.print(stack.pop());
        System.out.print(stack.pop());

        System.out.println("");

        stack.push("!");
        stack.push("World");
        stack.push(" ");
        stack.push("Hello");

        System.out.print(stack.pop());
        System.out.print(stack.pop());
        System.out.print(stack.pop());
        System.out.print(stack.pop());

        stack.push("Hello World!");
        System.out.println(stack.peek());
    }

    /**
     * Queues work with a FIFO process. This means that the first element added
     * to the queue, will be the first element removed from the queue. We use
     * the offer() method to add a value to the end of a queue (enqueue), and
     * the poll() method to remove the value at the top of the queue (dequeue).
     * We can also use the peek() method to read the element at the top of the
     * queue.
     */
    public static void queue() {
        Queue<String> queue = new LinkedBlockingQueue<>();

        queue.offer("Hello");
        queue.offer(" ");
        queue.offer("World");
        queue.offer("!");

        System.out.print(queue.poll());
        System.out.print(queue.poll());
        System.out.print(queue.poll());
        System.out.print(queue.poll());

        System.out.println("!");
        System.out.println("World");
        System.out.println(" ");
        System.out.println("Hello");

        System.out.print(queue.poll());
        System.out.print(queue.poll());
        System.out.print(queue.poll());
        System.out.print(queue.poll());

        queue.offer("Hello World!");
        System.out.println(queue.peek());
    }

    /**
     * Main driver
     * @param args 
     */
    public static void main(String[] args) {
        stack();
        queue();
    }
}